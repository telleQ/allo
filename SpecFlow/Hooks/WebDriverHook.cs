﻿using BoDi;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;

namespace SpecFlow
{
    [Binding]
    public class WebDriverHook
    {
        private readonly IObjectContainer _objectContainer;
        private IWebDriver _driver;

        public WebDriverHook(IObjectContainer objectContainer)
        {
            _objectContainer = objectContainer;
        }

        [BeforeScenario]
        public void CreateDriver()
        {
            _driver = new ChromeDriver(@"E:\chromedriver_win32");
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
            _driver.Manage().Window.Maximize();
            _objectContainer.RegisterInstanceAs<IWebDriver>(_driver);
        }

        [AfterScenario]
        public void KillDriver()
        {
            _driver.Quit();
        }
    }
}
