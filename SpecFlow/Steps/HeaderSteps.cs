﻿using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlow.POM;
using TechTalk.SpecFlow;

namespace SpecFlow.Steps
{
    [Binding]
    public class HeaderSteps
    {
        private IWebDriver _driver;
        private MainPage _mainPage;
        private ShipmentAndPaymentPage _shipmentAndPaymentPage;
        private CreditPage _creditPage;
        private StoresPage _storesPage;
        private DiscountsPage _discountPage;
        private OutletPage _outletPage;
        private TradeInPage _tradeInPage;
        private ContactUsPage _contactUsPage;

        public HeaderSteps(IWebDriver driver)
        {
            _driver = driver;
        }

        [Given(@"Allo website is open on Main page")]
        public void GivenAlloWebsiteIsOpenOnMainPage()
        {
            _driver.Navigate().GoToUrl("https://allo.ua/");
            _mainPage = new MainPage(_driver);
        }

        [When(@"User hover the cursor over the store phone number")]
        public void WhenUserHoverTheCursorOverTheStorePhoneNumber()
        {
            _mainPage.HoverOnPhoneLabel();
        }

        [When(@"User clicks on link Trade in")]
        public void WhenUserClicksOnLinkTradeIn()
        {
            _tradeInPage = _mainPage.ClickOnAlloTradeIn();
        }

        [When(@"User clicks on link Outlet")]
        public void WhenUserClicksOnLinkOutlet()
        {
            _outletPage = _mainPage.ClickOnOutlet();
        }

        [When(@"User clicks on link Discounts")]
        public void WhenUserClicksOnLinkDiscounts()
        {
            _discountPage = _mainPage.ClickOnDiscounts();
        }

        [When(@"User clicks on link Stores")]
        public void WhenUserClicksOnLinkStores()
        {
            _storesPage = _mainPage.ClickOnStores();
        }

        [When(@"User clicks on link Shipment and payment")]
        public void WhenUserClicksOnLinkShipmentAndPayment()
        {
            _shipmentAndPaymentPage = _mainPage.ClickOnShipmentAndPayment();
        }

        [When(@"User clicks on link Credit")]
        public void WhenUserClicksOnLinkCredit()
        {
            _creditPage = _mainPage.ClickOnCredit();
        }

        [Then(@"Store phone number contains ""(.*)""")]
        public void ThenStorePhoneNumberContains(string p0)
        {
            Assert.AreEqual(p0, _mainPage.GetTextFromElementPhoneLabel());
        }

        [Then(@"User sees label free ""(.*)""")]
        public void ThenUserSeesLabelFree(string p0)
        {
            Assert.AreEqual(p0, _mainPage.GetTextFromElementFreePhonesLabel());
        }

        [Then(@"User sees label paid ""(.*)""")]
        public void ThenUserSeesLabelPaid(string p0)
        {
            Assert.AreEqual(p0, _mainPage.GetTextFromElementPaidPhonesLabel());
        }

        [Then(@"Information block with free store phone numbers \((.*)\)(.*) is displayed to user")]
        public void ThenInformationBlockWithFreeStorePhoneNumbersIsDisplayedToUser(string p0, string p1)
        {
            Assert.AreEqual(p0, _mainPage.GetTextFromElementsWithPhones(_mainPage.FirstFreePhone));
            Assert.AreEqual(p1, _mainPage.GetTextFromElementsWithPhones(_mainPage.SecondFreePhone));
        }

        [Then(@"Information block with paid store phone numbers \((.*)(.*) is displayed to user")]
        public void ThenInformationBlockWithPaidStorePhoneNumbersIsDisplayedToUser(string p0, string p1)
        {
            Assert.AreEqual(p0, _mainPage.GetTextFromElementsWithPhones(_mainPage.FirstPaidPhone));
            Assert.AreEqual(p1, _mainPage.GetTextFromElementsWithPhones(_mainPage.SecondPaidPhone));
        }

        [Then(@"User sees information with text ""(.*)""")]
        public void ThenUserSeesInformationWithText(string p0)
        {
            Assert.AreEqual(p0, _mainPage.GetTextFromElementRequiredDescriptionByPhones());
        }

        [Then(@"User sees breadcrumbs on Trade in with text ""(.*)""")]
        public void ThenUserSeesBreadcrumbsOnTradeInWithText(string p0)
        {
            Assert.AreEqual(p0, _tradeInPage.GetTextFromElementTradeInLabel());
        }

        [Then(@"User sees label with text ""(.*)"" on Outlet")]
        public void ThenUserSeesLabelWithTextOnOutlet(string p0)
        {
            Assert.AreEqual(p0, _outletPage.GetTextFromElementOutletLabel());
        }

        [Then(@"User sees label with text ""(.*)"" on Discounts")]
        public void ThenUserSeesLabelWithTextOnDiscounts(string p0)
        {
            Assert.AreEqual(p0, _discountPage.GetTextFromElementDiscountsLabel());
        }

        [Then(@"User sees label with text ""(.*)"" on Stores")]
        public void ThenUserSeesLabelWithTextOnStores(string p0)
        {
            Assert.AreEqual(p0, _storesPage.GetTextFromElementStoresLabel());
        }

        [Then(@"User sees label with text ""(.*)"" on Shipment and payment")]
        public void ThenUserSeesLabelWithTextOnShipmentAndPayment(string p0)
        {
            Assert.AreEqual(p0, _shipmentAndPaymentPage.GetTextFromElementShipmentAndPaymentLabel());
        }

        [Then(@"User sees label with text ""(.*)"" on Credit")]
        public void ThenUserSeesLabelWithTextOnCredit(string p0)
        {
            Assert.AreEqual(p0, _creditPage.GetTextFromElementCreditLabel());
        }

        [When(@"User clicks on link Contact")]
        public void WhenUserClicksOnLinkContact()
        {
            _contactUsPage = _mainPage.ClickOnContactUs();
        }

        [Then(@"User sees label with text ""(.*)"" on Contact")]
        public void ThenUserSeesLabelWithTextOnContact(string p0)
        {
            Assert.AreEqual(p0, _contactUsPage.GetTextFromElementContactUsLabel());
        }


    }
}
