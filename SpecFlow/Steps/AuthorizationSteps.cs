﻿using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlow.POM;
using System;
using TechTalk.SpecFlow;

namespace SpecFlow.Steps
{
    [Binding]
    public class AuthorizationSteps
    {
        private IWebDriver _driver;
        private MainPage _mainPage;
        private AuthorizationPopup _loginPopup;

        public AuthorizationSteps(IWebDriver driver)
        {
            _driver = driver;
        }

        [When(@"User clicks on login button")]
        public void WhenUserClicksOnLoginButton()
        {
            _mainPage = new MainPage(_driver);
            _loginPopup = _mainPage.ClickOnLoginButton();
        }

        [When(@"User enter ""(.*)"" in field Phone or Email")]
        public void WhenUserEnterInFieldPhoneOrEmail(string p0)
        {
            _loginPopup.EnterTextToPhoneOrEmailField(p0);
        }

        [When(@"User enter ""(.*)"" in password's field")]
        public void WhenUserEnterInPasswordSField(string p0)
        {
            _loginPopup.EnterTextToPasswordField(p0);
        }

        [When(@"User clicks the enter button")]
        public void WhenUserClicksTheEnterButton()
        {
            _loginPopup.ClickOnLoginButtonInAuthorizationPopup();
        }

        [Then(@"User sees her name ""(.*)"" on the main page")]
        public void ThenUserSeesHerNameOnTheMainPage(string p0)
        {
            string textFromUserName = _mainPage.GetTextFromElementWithUserNameAfterLogin();
            Assert.AreEqual(p0, textFromUserName);
        }


        [Then(@"User sees validation error with text ""(.*)"" under phone's field")]
        public void ThenUserSeesValidationErrorWithTextUnderPhoneSField(string p0)
        {
            string textWithError = _loginPopup.GetTextFromElementWithErrorNotValidNumberInPhoneOrEmailField();
            Assert.AreEqual(p0, textWithError);
            
        }
        [Then(@"Validation error under phone's field has color ""(.*)""")]
        public void ThenValidationErrorUnderPhoneSFieldHasColor(string p0)
        {
            string colorTextWithError = _loginPopup.GetColorFromElementWithErrorNotValidNumberInPhoneOrEmailField();
            Assert.AreEqual(p0, colorTextWithError);
        }

        [Then(@"Validation error under password's field has color ""(.*)""")]
        public void ThenValidationErrorUnderPasswordSFieldHasColor(string p0)
        {
            string colorTextWithError = _loginPopup.GetColorFromElementWithErrorFieldIsRequiredToFill();
            Assert.AreEqual(p0, colorTextWithError);
        }


        [Then(@"User sees validation error with text ""(.*)"" under password's field")]
        public void ThenUserSeesValidationErrorWithTextUnderPasswordSField(string p0)
        {
            string textWithError = _loginPopup.GetTextFromElementWithErrorFieldIsRequiredToFill();
            Assert.AreEqual(p0, textWithError);
        }

    }
}
