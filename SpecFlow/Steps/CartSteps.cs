﻿using NUnit.Framework;
using OpenQA.Selenium;
using SpecFlow.POM;
using TechTalk.SpecFlow;

namespace SpecFlow.Steps
{
    [Binding]
    public class CartSteps
    {
        private IWebDriver _driver;

        private MainPage _mainPage;
        private CartPopup _cartPopup;

        public CartSteps(IWebDriver driver)
        {
            _driver = driver;
        }

        [Given(@"Allo website is open on Main Page")]
        public void GivenAlloWebsiteIsOpenOnMainPage()
        {
            _driver.Navigate().GoToUrl("https://allo.ua/");
            _mainPage = new MainPage(_driver);
        }

        [Given(@"User is not logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            Assert.AreEqual("Вхід", _mainPage.GetTextFromElementLoginButton());
        }
        
        [When(@"User clicks on cart")]
        public void WhenUserClicksOnCart()
        {
            _cartPopup = _mainPage.ClickOnCartButton();
        }
        
        [Then(@"Cart is empty")]
        public void ThenCartIsEmpty()
        {
            string cartLabel = _cartPopup.GetTextFromCartLabel();
            string cartSubLabel = _cartPopup.GetTextFromCartSubLabel();

            Assert.AreEqual("Кошик", cartLabel);
            Assert.AreEqual("Ваш кошик порожній.", cartSubLabel);
        }
    }
}
