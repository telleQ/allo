﻿Feature: Authorization
    As a user  
    I want to be able to sign in to a website
    In order to manage my subscriptions

    As a user  
    I want to see the error text under field
    In order to understand what needs to be fixed

Background:
	Given Allo website is open on Main Page

Scenario: Successful authorization with registered email
	When User clicks on login button
	And User enter "telleqs@gmail.com" in field Phone or Email
	And User enter "jxrblkzgr" in password's field
	And User clicks the enter button
	Then User sees her name "Анастасия" on the main page

Scenario: Failed authorization with not valid phone number
	When User clicks on login button
	And User enter "+380454543" in field Phone or Email
	And User clicks the enter button
	Then User sees validation error with text "Будь ласка, введіть правильний номер телефону." under phone's field
	And Validation error under phone's field has color "rgba(227, 24, 55, 1)"
	And User sees validation error with text "Це поле є обов'язковим." under password's field
	And Validation error under password's field has color "rgba(227, 24, 55, 1)"