﻿Feature: Cart
	As a user
	I want to access to my cart
	In order to view a content of my order any time

Scenario: Unauthorized user has empty cart
	Given Allo website is open on Main Page
	Given User is not logged in
	When User clicks on cart
	Then Cart is empty