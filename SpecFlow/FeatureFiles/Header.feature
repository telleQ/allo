﻿Feature: Header
    As a user  
    I want to see information about store phone numbers
	In order to call and resolve my issues with the store representative

	As a user  
    I want to use navigation
    In order to have quick access to different parts of the site

Background:
 Given Allo website is open on Main page 

Scenario Outline: Show the store phone numbers
	When User hover the cursor over the store phone number
	Then Store phone number contains "0 800 300 100"
	And User sees label free "Безкоштовні"
	And Information block with free store phone numbers <free> is displayed to user
	And User sees label paid "Згідно тарифу оператора"
	And Information block with paid store phone numbers <paid> is displayed to user
	And User sees information with text "*Дзвінки зі стаціонарних телефонів, а також мобільних операторів Київстар, Vodafone та Life, безкоштовні по всій Україні."

	Examples: 
	|	free				|	paid		        |
	|	((0-800) 200-100)	|	((056) 790-12-34)	|
	|	((0-800) 300-100)	|	((056) 797-63-23)	|

Scenario: Successful transition from Main page to Trade in page
	When User clicks on link Trade in
	Then User sees breadcrumbs on Trade in with text "Поміняй свій старий телефон на новий!"

Scenario: Successful transition from Main page to Outlet page
	When User clicks on link Outlet
	Then User sees label with text "РОЗПРОДАЖ ТОВАРІВ З УЦІНКОЮ" on Outlet

Scenario: Successful transition from Main page to Discounts page
	When User clicks on link Discounts
	Then User sees label with text "Акції" on Discounts

Scenario: Successful transition from Main page to Stores page
	When User clicks on link Stores
	Then User sees label with text "Адреси магазинів" on Stores

Scenario: Successful transition from Main page to Shipment and payment page
	When User clicks on link Shipment and payment
	Then User sees label with text "Доставка і оплата" on Shipment and payment

Scenario: Successful transition from Main page to Credit page
	When User clicks on link Credit
	Then User sees label with text "Кредит" on Credit

Scenario: Successful transition from Main page to Contact page
	When User clicks on link Contact
	Then User sees label with text "Консультації та замовлення за телефонами" on Contact