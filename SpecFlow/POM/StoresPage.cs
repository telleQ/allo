﻿using OpenQA.Selenium;

namespace SpecFlow.POM
{
    public class StoresPage
    {
        private IWebDriver _driver;

        public StoresPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public By StoresLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div[1]/div[1]/h1");

        public IWebElement FindElementStoresLabel()
        {
            return _driver.FindElement(StoresLabel);
        }

        public string GetTextFromElementStoresLabel()
        {
            return FindElementStoresLabel().Text;
        }
    }
}
