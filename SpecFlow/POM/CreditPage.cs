﻿using OpenQA.Selenium;

namespace SpecFlow.POM
{
    public class CreditPage
    {
        private IWebDriver _driver;

        public CreditPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public By CreditLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");

        public IWebElement FindElementCreditLabel()
        {
            return _driver.FindElement(CreditLabel);
        }

        public string GetTextFromElementCreditLabel()
        {
            return FindElementCreditLabel().Text;
        }
    }
}
