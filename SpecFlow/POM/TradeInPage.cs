﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace SpecFlow.POM
{
    public class TradeInPage
    {
        private IWebDriver _driver;

        public TradeInPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public By TradeInLabelFromBreadcrumbs = By.XPath("/html/body/div[3]/div[2]/div[1]/ul/li[2]/span");

        public IWebElement FindElementTradeInLabel()
        {
            return _driver.FindElement(TradeInLabelFromBreadcrumbs);
        }

        public string GetTextFromElementTradeInLabel()
        {
            return FindElementTradeInLabel().Text;
        }
    }
}
