﻿using OpenQA.Selenium;

namespace SpecFlow.POM
{
    public class CartPopup
    {
        private IWebDriver _driver;

        public By CartLabel = By.XPath("/html/body/div[3]/div/div/div[2]/span");
        public By CartSubLabel = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[1]");
        public By CartInformationBack = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]");
        public By CartLinkOnBack = By.XPath("/html/body/div[3]/div/div/div[3]/div/div[1]/div/p[2]/a");
        public By CartPopupCloseButton = By.XPath("/html/body/div[3]/div/div/div[1]");

        public CartPopup(IWebDriver driver)
        {
            _driver = driver;
        }

        public IWebElement FindCartLabel()
        {
            return _driver.FindElement(CartLabel);
        }

        public string GetTextFromCartLabel()
        {
            return FindCartLabel().Text;
        }

        public IWebElement FindCartSubLabel()
        {
            return _driver.FindElement(CartSubLabel);
        }

        public string GetTextFromCartSubLabel()
        {
            return FindCartSubLabel().Text;
        }

        public IWebElement FindCartInformationBack()
        {
            return _driver.FindElement(CartInformationBack);
        }

        public string GetTextFromCartInformationBack()
        {
            return FindCartInformationBack().Text;
        }

        public CartPopup ClickOnCartLinkOnBack()
        {
            _driver.FindElement(CartLinkOnBack).Click();
            return this;
        }

        public CartPopup ClickOnCartPopupCloseButton()
        {
            _driver.FindElement(CartPopupCloseButton).Click();
            return this;
        }

    }
}
