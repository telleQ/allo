﻿using OpenQA.Selenium;

namespace SpecFlow.POM
{
    public class ShipmentAndPaymentPage
    {
        private IWebDriver _driver;

        public ShipmentAndPaymentPage(IWebDriver driver)
        {
           _driver = driver;
        }

        public By ShipmentAndPaymentLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/h2");

        public IWebElement FindElementShipmentAndPaymentLabel()
        {
            return _driver.FindElement(ShipmentAndPaymentLabel);
        }

        public string GetTextFromElementShipmentAndPaymentLabel()
        {
            return FindElementShipmentAndPaymentLabel().Text;
        }
    }
}
