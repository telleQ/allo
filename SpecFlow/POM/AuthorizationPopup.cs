﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace SpecFlow.POM
{
    public class AuthorizationPopup
    {
        private IWebDriver _driver;

        public By AuthorizationPopupLoginLink = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[1]");
        public By AuthorizationPopupPhoneOrEmailField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[1]/input");
        public By AuthorizationPopupPasswordField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[1]/input");
        public By AuthorizationPopupHidePasswordButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/span");
        public By AuthorizationPopupForgetPasswordLink = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[2]/a");
        public By AuthorizationPopupLoginButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/button");

        public By AuthorizationPopupLoginByFacebookButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[3]/a[1]");
        public By AuthorizationPopupLoginByGoogleButton = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[2]/div[3]/a[2]");
        public By AuthorizationPopupLinkOnRegistrationOrLogin = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[2]/a");

        public By AuthorizationPopupRegistrationLink = By.XPath("/html/body/div[3]/div/div/div[2]/div/ul/li[2]");
        public By ErrorNotValidNumberInPhoneOrEmailField = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[1]/div[2]/div/span");
        public By ErrorFieldIsRequiredToFill = By.XPath("/html/body/div[3]/div/div/div[2]/div/div/div[1]/div[1]/form/div/div[1]/div[2]/div[2]/div/span");


        public AuthorizationPopup(IWebDriver driver)
        {
            _driver = driver;
        }

        public AuthorizationPopup EnterTextToPhoneOrEmailField(string data)
        {
            _driver.FindElement(AuthorizationPopupPhoneOrEmailField).SendKeys(data);
            return this;
        }

        public AuthorizationPopup EnterTextToPasswordField(string password)
        {
            _driver.FindElement(AuthorizationPopupPasswordField).SendKeys(password);
            return this;
        }

        public AuthorizationPopup ClickOnLoginButtonInAuthorizationPopup()
        {
            _driver.FindElement(AuthorizationPopupLoginButton).Click();
            return this;
        }

    
        public AuthorizationPopup ClickOnHidePasswordButtonInAuthorizationPopup()
        {
            _driver.FindElement(AuthorizationPopupHidePasswordButton).Click();
            return this;
        }

        public IWebElement FindElementWithErrorNotValidNumberInPhoneOrEmailField()
        {
            _driver.FindElement(ErrorNotValidNumberInPhoneOrEmailField);
            new WebDriverWait(_driver, TimeSpan.FromSeconds(15)).Until(ExpectedConditions.ElementIsVisible(ErrorNotValidNumberInPhoneOrEmailField));
            return _driver.FindElement(ErrorNotValidNumberInPhoneOrEmailField);
        }

        public string GetTextFromElementWithErrorNotValidNumberInPhoneOrEmailField()
        {
            return FindElementWithErrorNotValidNumberInPhoneOrEmailField().Text;
        }

        public string GetColorFromElementWithErrorNotValidNumberInPhoneOrEmailField()
        {
            return FindElementWithErrorNotValidNumberInPhoneOrEmailField().GetCssValue("color");
        }

        public IWebElement FindElementWithErrorFieldIsRequiredToFill()
        {
            return _driver.FindElement(ErrorFieldIsRequiredToFill);
        }

        public string GetTextFromElementWithErrorFieldIsRequiredToFill()
        {
            return FindElementWithErrorFieldIsRequiredToFill().Text;
        }

        public string GetColorFromElementWithErrorFieldIsRequiredToFill()
        {
            return FindElementWithErrorFieldIsRequiredToFill().GetCssValue("color");
        }
    }
}
