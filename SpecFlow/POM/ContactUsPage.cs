﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

namespace SpecFlow.POM
{
    public class ContactUsPage
    {
        private IWebDriver _driver;

        public ContactUsPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public By ContactUsLabel = By.XPath("/html/body/div[1]/div/div/div[2]/div/div/div/div/div/div[1]/h3");

        public IWebElement FindElementContactUsLabel()
        {
            return _driver.FindElement(ContactUsLabel);
        }

        public string GetTextFromElementContactUsLabel()
        {
            return FindElementContactUsLabel().Text;
        }
    }
}
