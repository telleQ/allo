﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace SpecFlow.POM
{
    public class MainPage
    {
        private IWebDriver _driver;
        private Actions _actions;

        public By LinkOnMainPage = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/a");

        // Header Menu First line.
        public By LinkOnBlog = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[1]/a");
        public By LinkOnFishka = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a");
        public By LinkOnVacancies = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[3]/a");
        public By LinkOnStores = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[4]/a");
        public By LinkOnShipmentAndPayment = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[5]/a");
        public By LinkOnCredit = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[6]/a");
        public By LinkOnWarrantyAndRefund = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[7]/a");
        public By LinkOnContactUs = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[1]/div/div[3]/ul/li[8]/a");

        // Header Menu Second Line.
        public By LinkOnSmartLive = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[1]/a");
        public By LinkOnAlloMoney = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[2]/a");
        public By LinkOnAlloUpgrade = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[3]/a");
        public By LinkOnAlloTradeIn  = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[4]/a");
        public By LinkOnOutlet = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[5]/a");
        public By LinkOnDiscounts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[2]/div/ul/li[6]/a");

        // Header Menu Third Line.
        public By CatalogueProductsButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/div");
        public By SearchInputField = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/label/input");
        public By SearchInputClearButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/button[2]");
        public By SearchInputSubmitButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[4]/div/div/form/button[3]");
        public By PhoneLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[1]");
        // Hover on PhoneLabel show additional elements.
        public By FreePhonesLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/ul/li[1]/strong");
        public By PaidPhonesLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/ul/li[2]/strong");
        public By FirstFreePhone = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/ul/li[1]/span[1]");
        public By SecondFreePhone = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/ul/li[1]/span[2]");
        public By FirstPaidPhone = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/ul/li[2]/span[1]");
        public By SecondPaidPhone = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/ul/li[2]/span[2]");
        public By RequiredDescriptionByPhones = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[2]/div/p");

        // Buttons on authorization.
        public By LoginRegistationButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div");
        public By LoginButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[1]");
        public By ElementWithUserNameAfterLogin = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/div[1]/span");
        public By RegistationButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[5]/div/div/button[2]");

        // Cart.
        public By CartButton = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div");
        public By CartLabel = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[2]/p");
        public By CartCountProducts = By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[6]/div/div[2]/div/span");

        public MainPage(IWebDriver driver)
        {
            _driver = driver;
            _actions = new Actions(driver);
        }

        public IWebElement FindElementWithUserNameAfterLogin()
        {
            _driver.FindElement(ElementWithUserNameAfterLogin);
            new WebDriverWait(_driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.InvisibilityOfElementWithText(LoginButton, "Анастасия"));
            return _driver.FindElement(ElementWithUserNameAfterLogin);
        }

        public string GetTextFromElementWithUserNameAfterLogin()
        {
            return FindElementWithUserNameAfterLogin().Text;
        }

        public IWebElement FindElementLoginButton()
        {
            return _driver.FindElement(LoginButton);
        }

        public string GetTextFromElementLoginButton()
        {
            return FindElementLoginButton().Text;
        }

        public AuthorizationPopup ClickOnLoginButton()
        {
            _driver.FindElement(LoginButton).Click();
            return new AuthorizationPopup(_driver);
        }

        public MainPage ClickOnRegistationButton()
        {
            _driver.FindElement(RegistationButton).Click();
            return this;
        }

        public CartPopup ClickOnCartButton()
        {
            _driver.FindElement(CartButton).Click();
            return new CartPopup(_driver);
        }

        public IWebElement FindElementWithPhoneLabel()
        {
            return _driver.FindElement(PhoneLabel);
        }

        public string GetTextFromElementPhoneLabel()
        {
            return FindElementWithPhoneLabel().Text;
        }
        public MainPage HoverOnPhoneLabel()
        {
            IWebElement phoneBlock = FindElementWithPhoneLabel();
            _actions.MoveToElement(_driver.FindElement(By.XPath("/html/body/div[1]/div/div/div[1]/div[1]/div[3]/div/ul/li[2]/div/div[1]")));
            _actions.Perform();
            new WebDriverWait(_driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementIsVisible(PhoneLabel));
            return this;
        }

        public IWebElement FindElementWithFreePhonesLabel()
        {
            return _driver.FindElement(FreePhonesLabel);
        }

        public string GetTextFromElementFreePhonesLabel()
        {
            return FindElementWithFreePhonesLabel().Text;
        }

        public IWebElement FindElementWithPaidPhonesLabel()
        {
            return _driver.FindElement(PaidPhonesLabel);
        }

        public string GetTextFromElementPaidPhonesLabel()
        {
            return FindElementWithPaidPhonesLabel().Text;
        }

        public IWebElement FindElementWithRequiredDescriptionByPhones()
        {
            return _driver.FindElement(RequiredDescriptionByPhones);
        }

        public string GetTextFromElementRequiredDescriptionByPhones()
        {
            return FindElementWithRequiredDescriptionByPhones().Text;
        }

        public string GetTextFromElementsWithPhones(By phoneLocator)
        {
            IWebElement element = _driver.FindElement(phoneLocator);
            new WebDriverWait(_driver, TimeSpan.FromSeconds(5)).Until(ExpectedConditions.ElementIsVisible(phoneLocator));
            return element.Text;
        }

        public TradeInPage ClickOnAlloTradeIn()
        {
            _driver.FindElement(LinkOnAlloTradeIn).Click();
            return new TradeInPage(_driver);
        }
        public OutletPage ClickOnOutlet()
        {
            _driver.FindElement(LinkOnOutlet).Click();
            return new OutletPage(_driver);
        }
        public StoresPage ClickOnStores()
        {
            _driver.FindElement(LinkOnStores).Click();
            return new StoresPage(_driver);
        }
        public CreditPage ClickOnCredit()
        {
            _driver.FindElement(LinkOnCredit).Click();
            return new CreditPage(_driver);
        }
        public ShipmentAndPaymentPage ClickOnShipmentAndPayment()
        {
            _driver.FindElement(LinkOnShipmentAndPayment).Click();
            return new ShipmentAndPaymentPage(_driver);
        }
        public DiscountsPage ClickOnDiscounts()
        {
            _driver.FindElement(LinkOnDiscounts).Click();
            return new DiscountsPage(_driver);
        }

        public ContactUsPage ClickOnContactUs()
        {
            _driver.FindElement(LinkOnContactUs).Click();
            return new ContactUsPage(_driver);
        }
    }
}
