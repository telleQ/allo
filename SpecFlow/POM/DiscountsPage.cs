﻿using OpenQA.Selenium;

namespace SpecFlow.POM
{
    public class DiscountsPage
    {
        private IWebDriver _driver;

        public DiscountsPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public By DiscountsLabel = By.XPath("/html/body/div[1]/div/div/div[2]/h1");

        public IWebElement FindElementDiscountsLabel()
        {
            return _driver.FindElement(DiscountsLabel);
        }

        public string GetTextFromElementDiscountsLabel()
        {
            return FindElementDiscountsLabel().Text;
        }
    }
}
