﻿using OpenQA.Selenium;

namespace SpecFlow.POM
{
    public class OutletPage
    {
        private IWebDriver _driver;

        public OutletPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public By OutletLabel = By.XPath("/html/body/div[1]/header/div[3]/div/h1");

        public IWebElement FindElementOutletLabel()
        {
            return _driver.FindElement(OutletLabel);
        }

        public string GetTextFromElementOutletLabel()
        {
            return FindElementOutletLabel().Text;
        }
    }
}
